﻿using Juan.Students.Context;
using Juan.Students.Contracts;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Juan.Students.WebAPI.Infraestructure
{
    public class DependencyResolverAPI : NinjectModule
    {
        public override void Load()
        {
            Bind<IStudentDao>().To<StudentDao>();
        }
    }
}