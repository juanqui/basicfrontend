﻿using Juan.Students.Entities;
using Juan.Students.Biz;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
//using System.Web.Http;
using System.IO;

namespace Juan.Students.WebAPI.Controllers
{
    public class StuentAPIController : BaseApiController
    {
        // GET: api/StuentAPI
        public IEnumerable<Student> Get()
        {
            return StudentManager.GetAllStudents();
        }

        // GET: api/StuentAPI/5
        //public Student Get(int id)
        public Student Get(string name)
        {
            // Student student = StudentManager.GetById(id);
            Student student = StudentManager.GetForName(name);
            if (student != null)
            {
                return student;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    
        // POST: api/StuentAPI
        public void Post(Student student)
        {
            if (student != null)
            {
                StudentManager.StoreStudent(student);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/StudentAPI/5
        // public Student Put(int id, [FromBody]Student studentUpdate)
        public Student Put(string name, [FromBody]Student studentUpdate)
        {
            //Student student = StudentManager.GetById(id);
            Student student = StudentManager.GetForName(name);
            if (student != null)
            {
                try
                {
                    // studentUpdate.id = student.id;
                    studentUpdate.Name = student.Name;
                    return StudentManager.StoreStudent(studentUpdate);
                }
                catch (Exception ex)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        // DELETE: api/StudentAPI/5
        //public void Delete(int id)
        public void Delete(string name)
        {
            //Student student = StudentManager.GetById(id);
            Student student = StudentManager.GetForName(name);
            if (student != null)
            {
                //StudentManager.DeleteStudent(student.id);
                StudentManager.DeleteStudent(student.Name);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
       
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api / upload")]
        public async Task<IHttpActionResult> Upload()
        {
            var folder = HostingEnvironment.MapPath("~/ Uploads");
            var provider = new MultipartFormDataStreamProvider(folder);
            var data = await Request.Content.ReadAsMultipartAsync(provider);
            return Ok();
        }
        ////////
        public Student[] LoadHighScores(string fileName)
        {
            string[] studentScoresText = File.ReadAllLines(fileName);
            Student[] studentScores = new Student[studentScoresText.Length];
            for (int index = 0; index < studentScoresText.Length; index++)
            {
                string[] tokens = studentScoresText[index].Split(',');

                string type = tokens[0];
                string name = tokens[1];
                string gender = tokens[2];
                string timestamp = tokens[3];

                Student st = new Student() { Type = type, Name = name, Gender = gender, Timestamp = timestamp };

                studentScores[index] = st;
            }
            return studentScores;
        }
    }
}