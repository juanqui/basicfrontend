﻿using Juan.Students.Biz;
using Juan.Students.WebAPI.Infraestructure;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Juan.Students.WebAPI.Controllers
{
    public class BaseApiController : ApiController
    {
        private static IKernel kernel = new StandardKernel(new DependencyResolverAPI());

        protected StudentManager StudentManager = kernel.Get<StudentManager>();
    }
}