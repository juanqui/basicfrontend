var Request = require('request');

describe('Server', () => {
    var server;
    beforeEach(() => {
        server = require('../myfirst');
        server.start;
    });
    afterEach(() => {
        server.close();
    });
    describe('GET /', () => {
        var data = {};
        beforeEach(done => {
            Request.get('http://localhost:3000/full', (error, response, body) => {
                data.status = response.statusCode;
                data.body = body;
                // console.log('status', data.status);
                // console.log('body', data.body);
                done();
            });
        });

        it('Body', () => {
            // console.log('aaaaaaaaaaaaaa 111');
            expect(data.body).toBe('The Polyglot Developer');
        });

        it('Status 200', () => {

            expect(data.status).toBe(200);
            // console.log('1111111111111111111111');
        });
    });
});