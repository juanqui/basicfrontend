var http = require('http');
var controller = require('./index');

const start = http.createServer(controller.app).listen(3000, 'localhost', () =>{
    console.log(`start server on port 3000 on localhost`);
});

const close = start.close();
module.exports = {
    start: start,
    close: function(){
        start.close();
    }
}