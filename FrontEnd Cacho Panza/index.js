const express = require('express')
const app = express()
const port = 3000
const min = 1;
const max = 6;
var die;

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function throwDies(min, max){
    die = [];
    die.push(getRandomInt(min, max));
    die.push(getRandomInt(min, max));
    die.push(getRandomInt(min, max));
    die.push(getRandomInt(min, max));
    die.push(getRandomInt(min, max));
    return die;
}

function equalCountDie(num, arrdie){
    var contResult = 0;
    for(let j = 0; j < arrdie.length; j++){
        if(num === die[j])//
        contResult++;
    }
    return contResult;
}

function totalResult(arrDies){
    var totalEquals = [];
    totalEquals[0] = equalCountDie(1, arrDies);
    totalEquals[1] = equalCountDie(2, arrDies);
    totalEquals[2] = equalCountDie(3, arrDies);
    totalEquals[3] = equalCountDie(4, arrDies);
    totalEquals[4] = equalCountDie(5, arrDies);
    totalEquals[5] = equalCountDie(6, arrDies);
   // console.log('TOTAL RESULT arrDies: '+totalEquals);
    return totalEquals;
}

function isFull(arrDies){ //3-2
    var myTotalResults = totalResult(arrDies);
    var tree = false;
    var two = false;
    var fullResult = false;
    for(var x = 0; x < myTotalResults.length; x++){
        if(myTotalResults[x] === 3)
            tree = true;
    }
    for(var y = 0; y < myTotalResults.length; y++){//ojo
        if(myTotalResults[y] === 2)
            two = true;
    }
    if(tree == true && two == true)
        fullResult = true;
    return fullResult;
}

function isPoker(arrDies){  //4-1
    var myTotalResults = totalResult(arrDies);
    var pokerResult = false;
    for(var x = 0; x <= myTotalResults.length; x++){
        if(myTotalResults[x] === 4)
            pokerResult = true;
    }
    return pokerResult;
}

function isStairs(arrDies){  // 12345 23456 13456
    var myTotalResults = totalResult(arrDies);
    var stairResult = false;
    if((myTotalResults[0] === 1 && myTotalResults[1] === 1 && myTotalResults[2] === 1 && myTotalResults[3] === 1 && myTotalResults[4] === 1) || (myTotalResults[1] === 1 && myTotalResults[2] === 1 && myTotalResults[3] === 1 && myTotalResults[4] === 1 && myTotalResults[5] === 1) || (myTotalResults[0] === 1 && myTotalResults[2] === 1 && myTotalResults[3] === 1 && myTotalResults[4] === 1 && myTotalResults[5] === 1))
        stairResult = true;
    return stairResult;
}

let playResult = {
    arrResult:[],
    message: ''
}

app.get('/full', (req, res) =>{    //3-2
    var myDies = throwDies(min, max);
    var totalRes = totalResult(myDies);
    var result = isFull(totalRes);
    playResult.arrResult = myDies;
    playResult.message = "full: " + result;
    res.status(200).send(playResult);
});

app.get('/poker', (req, res) =>{   //4-1
    var myDies = throwDies(min, max);
    var totalRes = totalResult(myDies);
    var result = isPoker(totalRes);
    playResult.arrResult = myDies;
    playResult.message = "poker: " + result;
    res.send(playResult);
});

app.get('/stairs', (req, res) =>{    // 12345 23456 13456
    var myDies = throwDies(min, max);
    var result = isStairs(myDies);
    playResult.arrResult = myDies;
    playResult.message = "stairs: " + result;
    res.send(playResult);
});

// app.listen(port, () => console.log(`Example app listening on port ${port}!`));
module.exports = {
    app: app
}