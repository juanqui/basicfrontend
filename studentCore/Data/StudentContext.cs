
using Microsoft.EntityFrameworkCore;
using studentCore.Entities;

namespace studentCore.Data {
    public partial class StudentContext: DbContext {


        
        public virtual DbSet<Student> Students {get; set;}
        public StudentContext(DbContextOptions<StudentContext> options): base(options){}
        protected override void OnModelCreating(ModelBuilder modelBuilder){
            modelBuilder.Entity<Student>(entity => {
                entity.HasIndex(i => i.Id)
                    .HasName("idx_student")
                    .IsUnique();

                entity.HasKey(k => k.Id)
                    .HasName("pk_student");
            });            
        }
    }
}