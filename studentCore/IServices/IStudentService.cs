using System.Collections.Generic;
using studentCore.Entities;

namespace studentCore.IServices {
    public interface IStudentService
    {       
        IEnumerable<Student> GetAll();
        Student GetById(int id);
        bool Create(Student position);
        bool Delete(int id);
    }
}