using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using studentCore.Data;
using studentCore.Entities;
using studentCore.IServices;

namespace studentCore.Services{
    public class StudentService : IStudentService
    {
         private StudentContext db;

        public StudentService(StudentContext db){
            this.db = db;
        }
        public bool Create(Student student)
        {
            bool resp = false;
            try{
                student.Id = this.GetMaxId() + 1;
                student.Enabled = true;
                student.Timestamp = DateTime.Now.ToString();
                db.Add(student);
                db.SaveChanges();
                resp = true;
            }catch(DbUpdateException){
                throw new System.Exception("Error creating a position");
            }
            return resp;
        }

        public bool Delete(int id)
        {
            bool resp = false;
            
            try{                
                Student student = db.Students.Where(x => x.Id == id).FirstOrDefault();
                student.Enabled = false; 

                if (student != null){
                   //db.Entry(student).CurrentValues.SetValues(student);
                   db.Students.Update(student);
                   db.SaveChanges();
               }
                resp = true;
            }catch(Exception ex){
                throw new System.Exception("Error creating a student, "+ ex.Message);
            }
            return resp;
        }

        public IEnumerable<Student> GetAll()
        {
            return this.db.Students.AsEnumerable()??null;
        }

        public Student GetById(int id)
        {
            return this.db.Students.Where(x => x.Id == id).FirstOrDefault()??null;
        }

        private int GetMaxId(){
            int id = 0;
            try{
                IEnumerable<int> ids = db.Students.Select(x => x.Id).ToList<int>();
                if (ids.Count() > 0){
                    id = ids.Max();
                }
            }catch(Exception ex){
                throw new Exception(ex.Message + ", Inner exception: " + ex.InnerException.ToString());
            }
            return id;
        }
    }
}