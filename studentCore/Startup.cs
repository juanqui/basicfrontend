﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using studentCore.Data;
using studentCore.IServices;
using studentCore.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace studentCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<StudentContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection"))
            );

             #region 
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Student Core",
                    Description = "REST API",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Juan Carlos Pinto", Email = "juancarlos.pinto.avantica@gmail.com", Url = "" },
                    License = new License { Name = "", Url = "https://example.com/license" }
                });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";                
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile); 
                c.IncludeXmlComments(xmlPath);
            });
            #endregion  
            services.AddScoped<IStudentService, StudentService>();
            services.AddCors();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }


            // reconfigurar en lo posterior, para un dominio especifico, y debe ser en config
           // app.UseCors("PermitirOrigenEspecifico");
           app.UseCors(builder =>{
               builder.AllowAnyHeader();
               builder.AllowAnyMethod();
               builder.AllowAnyOrigin();
           });

           #region Enable the static files middleware
           app.UseStaticFiles();
           #endregion          

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
