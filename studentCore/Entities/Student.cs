namespace studentCore.Entities {
    public class Student {
        public int Id {get; set;}
        public string Type { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Timestamp { get; set; }//in other format
        public bool Enabled {get; set; }
    }
}