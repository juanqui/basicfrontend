using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using studentCore.Entities;
using studentCore.IServices;

namespace studentCore.Controllers
{
    [Route("studentApi/[controller]")]
    public class StudentController : Controller
    {
        private IStudentService stService;
        public StudentController(IStudentService studentService){
            this.stService = studentService;
        }

        [HttpPost]
        [Produces("application/json", Type = typeof(bool))]
        public IActionResult Post([FromBody]Student position){
            return Ok(this.stService.Create(position));
        }
        [HttpGet]
        [Produces("application/json", Type = typeof(IEnumerable<Student>))]
        public IActionResult Get(){
            return Ok(this.stService.GetAll());
        }

        [HttpGet("{id}")]
        [Produces("application/json", Type = typeof(Student))]
        public IActionResult Get(int id){
            return Ok(this.stService.GetById(id));
        }
        [HttpPut("{id}")]
        [Produces("application/json", Type = typeof(bool))]
        public IActionResult Put(int id){
            return Ok(this.stService.Delete(id));
        }
    }
}