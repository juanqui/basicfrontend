import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/model/student';
import { StudentsService } from 'src/app/services/list.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [
    StudentsService
  ]
})
export class CreateComponent implements OnInit {

  student:Student;
  constructor(
    private studentService: StudentsService
  ) { }

  ngOnInit() {
    this.init();
  }

  init(){
    this.student = new Student();
    this.student.gender = 'male';
    this.student.type = 'kinder';
  }

  onChangeType(data: any){
    this.student.type = data;
  }

  save(){
    this.studentService.add(this.student)
      .subscribe((res: any) =>{
        console.log("Created");
      });
  }
}
