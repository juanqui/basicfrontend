import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [CreateComponent, ListComponent],
  imports: [
    CommonModule,
    FormsModule,
    DataTablesModule
  ]
})
export class StudentsModule { }
