import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Student } from 'src/app/model/student';
import { StudentsService } from 'src/app/services/list.service';
import * as $ from 'jquery';
import 'datatables.net';

@Component({
  selector: '[app-list]',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [
    StudentsService
  ]
})
export class ListComponent implements OnInit {
  dtOptions: DataTables.Settings = {};

  students: Student[];
  constructor(
    private studentService: StudentsService
  ) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      responsive: true,
      info: true,
      searching: true
      // language: {
      //   url: '//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
      // }
    };
    this.loadStudents();
  }
  loadStudents() {
    this.studentService.getAll()
      .subscribe((resp: Student[]) => {

        this.students = resp.filter(x => x.enabled === true);
        console.log(JSON.stringify(this.students))
      });
  }

  onDelete(id: number) {
    this.studentService.delete(id)
      .subscribe((res: boolean) => {
        console.log("Deleted", res);
        this.loadStudents();
      });
  }
}
