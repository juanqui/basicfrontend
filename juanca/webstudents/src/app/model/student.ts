export class Student {
    id: number;
    type: string;
    name: string;
    gender: string;
    timestamp: string;
    enabled: boolean;
}