import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './students/create/create.component';
import { SRoutes } from './routes';
import { ListComponent } from './students/list/list.component';

const routes: Routes = [
  { path: SRoutes.CREATE, component: CreateComponent},
  { path: SRoutes.LIST, component: ListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
