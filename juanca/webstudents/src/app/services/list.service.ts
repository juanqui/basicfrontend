import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../model/student';

const LIST_API = 'http://localhost:5080/studentApi/student';
@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private http: HttpClient) { }

  getAll():Observable<Student[]>{
    return this.http.get<Student[]>(LIST_API);
  }

  add(student: Student):Observable<any>{
    console.log("aaaa:",JSON.stringify(student))
    return this.http.post<any>(LIST_API, student);
  }

  delete(id:number):Observable<boolean>{
    return this.http.put<boolean>(`${LIST_API}/${id}`, id);
  }
}

